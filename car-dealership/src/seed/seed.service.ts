import {Injectable} from '@nestjs/common';
import {CAR_SEED} from "./data/cars.seed";
import {BRANDS_SEED} from "./data/brands.seed";
import {CarsService} from "../cars/cars.service";
import {BrandsService} from "../brands/brands.service";

@Injectable()
export class SeedService {

    constructor(private readonly carService: CarsService,
                private readonly brandService: BrandsService) {
    }

    populateDB() {
        this.carService.fillCarsWithSeedData(CAR_SEED)
        this.brandService.fillBrandsWithSeedData(BRANDS_SEED)
        return 'Seed Sucess Fully!'
    }

}
