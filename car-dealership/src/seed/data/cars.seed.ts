import {v4 as uuid} from 'uuid';
import {Car} from "../../interfaces/car.interface";

export  const CAR_SEED: Car[] = [
    {
        id: uuid(),
        brand: 'Toyota',
        model: 'Corolla'
    },
    {
        id: uuid(),
        brand: 'Toyota',
        model: 'Civic'
    },{
        id: uuid(),
        brand: 'Jeep',
        model:'Cherokee'
    }
]