import {BadRequestException, Injectable, NotFoundException} from '@nestjs/common';
import {v4 as uuid} from 'uuid';
import {Car} from "../interfaces/car.interface";
import {CreateCarDto, UpdateCarDto} from "./dto";


@Injectable()
export class CarsService {
    private cars: Car[] = [];

    findAll(){
        return this.cars;
    }

    findOneById(id: string){
        const car = this.cars.find(car => car.id  === id);
        if(!car){
            throw new NotFoundException (`Not found product.`);
        }
        return car;
    }

    create( createCartDTO: CreateCarDto){
        const car: Car={
            id: uuid(),
           ...createCartDTO
        }

        this.cars.push(car);

        return car;
    }


    update(id: string, updateCartDTO: UpdateCarDto){

        let carDB = this.findOneById(id);

        if(updateCartDTO.id && updateCartDTO.id !== id) throw new BadRequestException('Car ids not valid!!')

        this.cars =  this.cars.map(car => {
            if(car.id === id){
                carDB = {
                    ...carDB,
                    ...updateCartDTO,
                    id
                }
                return carDB;
            }
            return car;
        });

        return carDB;
    }

    delete(id: string){
        let validateId =  this.cars.some(car => car.id === id);
        if(!validateId) throw new NotFoundException('Car id not found');

        let result = this.cars.filter(car => car.id !== id);
        this.cars =  result;
        return result;

    }

    fillCarsWithSeedData(cars: Car[]){
        this.cars = cars;
    }


}
