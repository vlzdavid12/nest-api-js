import './style.css';
import {connectToServer} from "./socket-client";

document.querySelector<HTMLDivElement>('#app')!.innerHTML = `
  <div class="continer">
  <h1>WebSocket Client</h1>
  <input id="jwt_token" placeholder="Json Web Token" />
  <button id="btn_connect" style="padding: 10px">Connect</button>
  <br/>
  <span id="server-status"></span>
  <ul id="clients-url">
  </ul>
    <form id="message-form">
    <input placeholder="message" id="message-input" />
    </form>
  <h3>Messages</h3>
  <ul id="messages" ></ul>
  </div>
  
`
const inputJwt = document.querySelector<HTMLInputElement>('#jwt_token');
const btnConnect = document.querySelector<HTMLButtonElement>('#btn_connect');

btnConnect!.addEventListener('click', () => {

    if(inputJwt!.value.trim().length <= 0){
        return alert("Enter Token by new client");
    }
    connectToServer(inputJwt!.value.trim());
});

