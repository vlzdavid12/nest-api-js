import {BeforeInsert, BeforeUpdate, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {ProductImage} from "./index";
import {User} from "../../auth/entities/user.entity";
import {ApiProperty} from "@nestjs/swagger";

@Entity({name: 'products'})
export class Product {
    @ApiProperty({
        example: '08ea1012-6aae-4505-8d96-e1be060097c1',
        description: "Product ID",
        uniqueItems: true
    })
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @ApiProperty({
        example: 'T-Shit Teslo',
        description: "Product Title",
        uniqueItems: true
    })
    @Column('text', {
        unique: true,
    })
    title: string;

    @ApiProperty({
        example: 0,
        description: "Product Price",
    })
    @Column('float', {
        default: 0
    })
    price: number;

    @ApiProperty()
    @ApiProperty({
        example: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        description: "Product Description",
        default: null,
    })
    @Column({
        type: 'text',
        nullable: true
    })
    description: string;

    @ApiProperty({
        example: 'mi_product',
        description: "Product Slug - for SEO",
        uniqueItems: true
    })
    @ApiProperty()
    @Column('text', {
        unique: true
    })
    slug: string;

    @ApiProperty({
        example: '10',
        description: "Product STOCK",
        default: 0
    })
    @ApiProperty()
    @Column('int', {
        default: 0
    })
    stock: number;

    @ApiProperty({
        example: ['M', 'XL', 'L'],
        description: "Product Sizes",
        default: 0
    })
    @Column('text', {
        array: true
    })
    sizes: string[]

    @ApiProperty({
        example: "women",
        description: "Product gender",
        default: 0
    })
    @Column('text')
    gender: string

    @ApiProperty()
    @Column('text',{
        array:  true,
        default: []
    })
    tags: string[];

    @OneToMany(
        () => ProductImage,
        productImage => productImage.product,
        {cascade: true, eager: true}
    )
    images?: ProductImage[]


    @ManyToOne(
        () => User,
        (user) => user.product,
{eager: true}
    )
    user: User

    @BeforeInsert()
    checkSlugInsert() {
        if (!this.slug) {
                 this.slug = this.title;
        }
        this.slug =  this.slug.toLowerCase()
            .replaceAll(" ", "_")
            .replaceAll(",", "_")
    }

    @BeforeUpdate()
    checkSlugUpdate() {
        this.slug =  this.slug.toLowerCase()
            .replaceAll(" ", "_")
            .replaceAll(",", "_")
    }

}
