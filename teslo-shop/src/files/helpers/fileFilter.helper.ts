export const fileFilter = (req : Express.Request, file: Express.Multer.File, callback: Function) => {
    if(!file) return callback(new Error("File is Empty"), false);
    const fileFormat = file.mimetype.split('/')[1];
    const validateExtension = ['jpg', 'jpeg', 'png', 'gif'];

    if(validateExtension.includes(fileFormat)){
        return callback(null, true);
    }

    return callback(null, false);
}