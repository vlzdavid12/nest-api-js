import {IsOptional, IsPositive} from "class-validator";
import {Type} from "class-transformer";
import {ApiProperty} from "@nestjs/swagger";

export class PaginationDto{
    @ApiProperty({
        default: 10 , description: "How many rows do yuo need."
    })
    @IsOptional()
    @IsPositive()
    @Type(()=> Number)  // enableImplicitConversions: true
    limit?:number;
    @ApiProperty({
        default: 0 , description: "How many rows do yuo to skip."
    })
    @IsOptional()
    @Type(()=> Number)
    offset?: number;
}