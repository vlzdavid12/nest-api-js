import {Body, Controller, Get, Headers, Post, UseGuards} from '@nestjs/common';
import {ApiTags} from "@nestjs/swagger";
import {AuthService} from './auth.service';
import {CreateUserDto, LoginUserDto} from './dto/'
import {AuthGuard} from "@nestjs/passport";
import {User} from "./entities/user.entity";
import {Auth, GetUser, RawHeaders, RoleProtected} from "./decorators/index";
import {IncomingHttpHeaders} from "http";
import {UserRoleGuard} from "./guards/user-role.guard";
import {ValidRoles} from "./interfaces";


@ApiTags('Auth')
@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {
    }

    @Post('register')
    create(@Body() createAuthDto: CreateUserDto) {
        return this.authService.create(createAuthDto);
    }

    @Post('login')
    loginUser(@Body() loginUserDto: LoginUserDto) {
        return this.authService.login(loginUserDto);
    }


    @Get('check-status')
    @Auth(ValidRoles.user)
    checkAuthStatus(
        @GetUser() user: User
    ){
        return this.authService.checkAuthStatus(user)
    }

    @Get('private')
    @UseGuards(AuthGuard())
    testingPrivateRoute(
        // @Req() request: Express.Request
        @GetUser() user: User,
        @GetUser('email') userEmail: string,
        @RawHeaders() rawHeaders: string[],
        @Headers() headers: IncomingHttpHeaders
    ) {
        // console.log({user: request.user})
        return {
            ok: true,
            message: "Hello World Private",
            user,
            userEmail,
            rawHeaders,
            headers
        }
    }

    @Get('private2')
    @RoleProtected(ValidRoles.superUser, ValidRoles.admin)
    //@SetMetadata('roles', ['admin', 'super-user'])
    @UseGuards(AuthGuard(), UserRoleGuard)
    testingPrivateRuteTwo(
        @GetUser() user: User
    ){
        return {
            ok: true,
            user
        }
    }


    @Get('private3')
    @Auth(ValidRoles.admin, ValidRoles.superUser )
    testingPrivateRuteThree(
        @GetUser() user: User
    ){
        return {
            ok: true,
            user
        }
    }



}
