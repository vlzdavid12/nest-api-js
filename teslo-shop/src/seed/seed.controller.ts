import {Controller, Get} from '@nestjs/common';
import {ApiTags} from "@nestjs/swagger";
import {SeedService} from './seed.service';
import {Auth} from "../auth/decorators";
import {ValidRoles} from "../auth/interfaces";

@ApiTags('Seeds')
@Controller('seed')
export class SeedController {
  constructor(private readonly seedService: SeedService) {}

  @Get()
  //@Auth(ValidRoles.admin, ValidRoles.superUser)
  executeSeed() {
    this.seedService.runSeed();
    return 'SEED EXECUTED...';
  }

}
